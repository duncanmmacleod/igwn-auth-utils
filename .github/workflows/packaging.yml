# ---------------------------
#
# Prepare distributions of this project
# for various platforms
#
# ---------------------------

name: Packaging

on:
  push:
    branches:
      - main
      - master
  pull_request:
    branches:
      - main
      - master

jobs:
  tarball:
    name: Tarball

    runs-on: ubuntu-latest

    steps:
      - name: Get source code
        uses: actions/checkout@v2
        with:
          fetch-depth: 0

      - name: Set up Python
        uses: actions/setup-python@v2
        with:
          python-version: '3.x'

      - name: Install build requirements
        run: python -m pip install build

      - name: Create distributions
        run: python -m build --outdir . --sdist .

      - uses: actions/upload-artifact@v2
        with:
          name: tarball
          path: igwn-auth-utils-*.tar.*
          if-no-files-found: error

  # -- Debian ---------------

  debian-source:
    name: LSCSoft ${{ matrix.debian }} source package
    needs:
      - tarball
    strategy:
      fail-fast: false
      matrix:
        debian:
          - buster
          - bullseye
    runs-on: ubuntu-latest
    container: igwn/base:${{ matrix.debian }}
    env:
      TARBALL: "igwn-auth-utils-*.tar.*"
    steps:
      - name: Download tarball
        uses: actions/download-artifact@v2
        with:
          name: tarball

      - name: Configure apt
        run: |
          apt-get autoclean
          apt-get -y -q -q update
          apt-get -y -q -q install \
              devscripts \
              dpkg-dev \
          ;

      - name: Create source package
        run: |
          # extract tarball to src/
          mkdir src
          tar -xf ${TARBALL} -C src --strip-components=1
          # get versions from the Python metadata and the Debian changelog
          PKG_VERSION=$(grep ^Version src/PKG-INFO | cut -d\  -f2)
          CHANGELOG_VERSION=$(dpkg-parsechangelog -l src/debian/changelog --show-field Version)
          # and compare them:
          USE_PKG_VERSION=$(python3 -c "print(str('${PKG_VERSION}' > '${CHANGELOG_VERSION}').lower())")
          # if the package version is newer than the changelog version
          # (standard dev update), we need to add a changelog entry
          if ${USE_PKG_VERSION}; then
              (cd src; dch --newversion ${PKG_VERSION}-9999 --controlmaint "ci build");
          else
              PKG_VERSION=${CHANGELOG_VERSION%%-*}
          fi
          # rename tarball for debian orig
          _orig=$(
              basename ${TARBALL} | \
              sed 's|\(.*\)-\(.*\).\(tar\..*\)|\1_'${PKG_VERSION}'.orig.tar.gz|'
          )
          mv -v ${TARBALL} "${_orig}"
          # create debian source package files
          dpkg-source --build src

      - uses: actions/upload-artifact@v2
        with:
          name: dsc-${{ matrix.debian }}
          path: |
            *.orig.tar.*
            *.debian.tar.*
            *.dsc
          if-no-files-found: error

  debian-binary:
    name: LSCSoft ${{ matrix.debian }} binary package
    needs:
      - debian-source
    strategy:
      fail-fast: false
      matrix:
        debian:
          - buster
          - bullseye
    runs-on: ubuntu-latest
    container: igwn/base:${{ matrix.debian }}
    env:
      DSC: "igwn-auth-utils_*.dsc"
    steps:
      - name: Download source package
        uses: actions/download-artifact@v2
        with:
          name: dsc-${{ matrix.debian }}

      - name: Configure apt
        run: |
          apt-get -y -q -q update
          apt-get -y -q -q install \
              devscripts \
              dpkg-dev \
          ;

      - name: Unpack DSC
        run: |
          dpkg-source --extract ${DSC} src

      - name: Install build dependencies
        shell: bash -ex {0}
        run: |
          cd src
          mk-build-deps \
              --tool "apt-get -y -q -o Debug::pkgProblemResolver=yes --no-install-recommends" \
              --install \
              --remove \
          ;

      - name: Build binary packages
        run: |
          cd src
          # build debian packages
          dpkg-buildpackage -us -uc -b

      - name: Print package info
        run: |
          # print contents of packages
          for debf in *.deb; do
              echo "===== ${debf}"
              dpkg --info "${debf}"
              dpkg --contents "${debf}"
          done

      - uses: actions/upload-artifact@v2
        with:
          name: deb-${{ matrix.debian }}
          path: |
            *.buildinfo
            *.changes
            *.deb
          if-no-files-found: error

  debian-install:
    name: LSCSoft ${{ matrix.debian }} install test
    needs:
      - debian-binary
    strategy:
      fail-fast: false
      matrix:
        debian:
          - buster
          - bullseye
    runs-on: ubuntu-latest
    container: igwn/base:${{ matrix.debian }}
    steps:
      - name: Download binary packages
        uses: actions/download-artifact@v2
        with:
          name: deb-${{ matrix.debian }}

      - name: Configure apt
        run: apt-get -y -q -q update

      - name: Install packages
        shell: bash -ex {0}
        run: |
          dpkg --install *.deb || { \
              apt-get -y -f install;
              dpkg --install *.deb;
          }

  lint-debian:
    name: Lint Debian packages
    needs:
      - debian-binary
    strategy:
      fail-fast: false
      matrix:
        debian:
          - buster
          - bullseye
    runs-on: ubuntu-latest
    container: debian:${{ matrix.debian }}
    steps:
      - name: Download debian package
        uses: actions/download-artifact@v2
        with:
          name: deb-${{ matrix.debian }}

      - name: Install lintian
        run: |
          apt-get -y -q -q update
          apt-get -y -q -q install \
              lintian \
          ;

      - name: Lintian
        if: matrix.debian == 'buster'
        run: lintian --color=auto --fail-on-warnings --allow-root --pedantic --suppress-tags manpage-has-useless-whatis-entry,manpage-has-errors-from-man *.changes

      - name: Lintian
        if: matrix.debian == 'bullseye'
        run: lintian --color=auto --fail-on warning --allow-root --pedantic --suppress-tags manpage-has-useless-whatis-entry,manpage-has-errors-from-man *.changes

  # -- RHEL -----------------

  rhel-source:
    name: LSCSoft ${{ matrix.el }} source package
    needs:
      - tarball
    strategy:
      fail-fast: false
      matrix:
        el:
          - el7-testing
          - el8-testing
    runs-on: ubuntu-latest
    container: igwn/base:${{ matrix.el }}
    env:
      TARBALL: "igwn-auth-utils-*.tar.*"
    steps:
      - name: Download tarball
        uses: actions/download-artifact@v2
        with:
          name: tarball

      - name: Configure DNF
        if: matrix.el == 'el7-testing'
        run: ln -s /usr/bin/yum /usr/bin/dnf

      - name: Configure EPEL
        run: |
          dnf -y install epel-release
          dnf -y install epel-rpm-macros

      - name: Configure rpmbuild
        run: |
          dnf -y install \
              python-srpm-macros \
              rpm-build \
          ;

      - name: Extract spec file and update version
        run: |
          tar --file ${TARBALL} --wildcards --strip-components 1 --get igwn-auth-utils*/*.spec igwn-auth-utils*/PKG-INFO
          PKG_VERSION=$(grep ^Version PKG-INFO | cut -d\  -f2)
          sed -i 's|define version\( *\)\(.*\)|define unmangled_version '${PKG_VERSION}'\n%define version '${PKG_VERSION/-/+}'|' *.spec
          sed -i 's|pypi_source|pypi_source %{name} %{unmangled_version}|' *.spec

      - name: Create source package
        run: rpmbuild -bs --define "_srcrpmdir $(pwd)" --define "_sourcedir $(pwd)" *.spec

      - uses: actions/upload-artifact@v2
        with:
          name: srpm-${{ matrix.el }}
          path: "*.src.rpm"
          if-no-files-found: error

  rhel-binary:
    name: LSCSoft ${{ matrix.el }} binary package
    needs:
      - rhel-source
    strategy:
      fail-fast: false
      matrix:
        el:
          - el7-testing
          - el8-testing
    runs-on: ubuntu-latest
    container: igwn/base:${{ matrix.el }}
    env:
      SRPM: "igwn-auth-utils-*.src.rpm"
    steps:
      - name: Download SRPM
        uses: actions/download-artifact@v2
        with:
          name: srpm-${{ matrix.el }}

      - name: Configure DNF
        if: matrix.el == 'el7-testing'
        run: ln -s /usr/bin/yum /usr/bin/dnf

      - name: Configure EPEL
        run: |
          dnf -y install epel-release
          dnf -y install epel-rpm-macros

      - name: Install build tools (EL7)
        if: matrix.el == 'el7-testing'
        run: |
          dnf -y -q install \
              rpm-build \
              yum-utils \
          ;

      - name: Install build tools (EL8+)
        if: matrix.el != 'el7-testing'
        run: |
          dnf -y -q install \
              rpm-build \
              "dnf-command(builddep)" \
          ;

      - name: Install build dependencies (EL7)
        if: matrix.el == 'el7-testing'
        run: yum-builddep -y ${SRPM}

      - name: Install build dependencies (EL8+)
        if: matrix.el == 'el8-testing'
        run: dnf builddep -y ${SRPM}

      - name: Build binary packages
        run: |
          rpmbuild --rebuild --define "_rpmdir $(pwd)" ${SRPM}
          rm -f ${SRPM}
          mv */*.rpm .

      - name: Print package info
        run: |
          # print contents of packages
          for rpmf in *.rpm; do
              echo "===== ${rpmf}"
              rpm -qlp "${rpmf}"
              echo "Files:"
              rpm -qip "${rpmf}"
              echo "Provides:"
              rpm -qp --provides "${rpmf}"
              echo "Requires:"
              rpm -qp --requires "${rpmf}"
          done

      - uses: actions/upload-artifact@v2
        with:
          name: rpm-${{ matrix.el }}
          path: "*.rpm"
          if-no-files-found: error

  rhel-install:
    name: LSCSoft ${{ matrix.el }} install test
    needs:
      - rhel-binary
    strategy:
      fail-fast: false
      matrix:
        el:
          - el7-testing
          - el8-testing
    runs-on: ubuntu-latest
    container: igwn/base:${{ matrix.el }}
    steps:
      - name: Download RPMs
        uses: actions/download-artifact@v2
        with:
          name: rpm-${{ matrix.el }}

      - name: Configure DNF
        if: matrix.el == 'el7-testing'
        run: ln -s /usr/bin/yum /usr/bin/dnf

      - name: Configure EPEL
        run: |
          dnf -y install epel-release
          dnf -y install epel-rpm-macros

      - name: Install RPMs
        run: dnf -y install *.rpm

  lint-rhel:
    name: Lint RPMs
    needs:
      - rhel-binary
    strategy:
      fail-fast: false
      matrix:
        el:
          - 7
          - 8
    runs-on: ubuntu-latest
    container: rockylinux:8
    steps:
      - name: Download RPM
        uses: actions/download-artifact@v2
        with:
          name: rpm-el${{ matrix.el }}-testing

      - name: Install rpmlint
        run: |
          dnf -y -q install \
              rpmlint \
          ;

      - name: Prepare rpmlintrc
        run : |
          cat << EOF > rpmlintrc
          # don't validate Source0
          setOption("NetworkEnabled", False)
          EOF

      - name: Lint
        run: rpmlint -f rpmlintrc --info *.rpm
